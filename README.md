[![coverage](https://gitlab.com/niuz/logging-go/badges/master/coverage.svg?job=unit_tests)](https://gitlab.com/niuz/logging-go/commits/master)

# Logging Library for Niuz Go projects

```go
package main

import "context"
import "gitlab.com/niuz/logging-go"

logger = logging.GetLogger()

func main() {
	var logger = logging.GetLogger()
	var loggerCtx = logging.NewContext(
		context.Background(),
		map[string]interface{}{"correlationID": "..."})
	
	logger = logger.WithContext(loggerCtx)
	
	logger.Info("This is info")
	logger.Debug("This is debug")
	logger.Error("This is error")
}
```
