package logging

import (
	"context"
	"github.com/sirupsen/logrus"
	"os"
	"runtime"
	"strings"
	"sync"
)

type loggerKeyType int

type Fields logrus.Fields

type Logger struct {
	Log    logrus.Logger
	Fields Fields
}

type Level uint32

const loggerKey loggerKeyType = iota

const (
	// PanicLevel level, highest level of severity. Logs and then calls panic with the
	// message passed to Debug, Info, ...
	PanicLevel Level = iota
	// FatalLevel level. Logs and then calls `os.Exit(1)`. It will exit even if the
	// logging level is set to Panic.
	FatalLevel
	// ErrorLevel level. Logs. Used for errors that should definitely be noted.
	// Commonly used for hooks to send errors to an error tracking service.
	ErrorLevel
	// WarnLevel level. Non-critical entries that deserve eyes.
	WarnLevel
	// InfoLevel level. General operational entries about what's going on inside the
	// application.
	InfoLevel
	// DebugLevel level. Usually only enabled when debugging. Very verbose logging.
	DebugLevel
)

var log *logrus.Logger
var once sync.Once

func configLogger(level Level) (logrusLog *logrus.Logger) {

	logrusLog = logrus.New()

	logrusLog.Formatter = &logrus.JSONFormatter{}
	logrusLog.Out = os.Stdout
	logrusLog.Level = logrus.Level(level)
	logrusLog.SetNoLock()

	return
}

func SetUp(level Level) {
	once.Do(func() {
		log = configLogger(level)
	})
}

func GetLogger() *Logger {
	SetUp(InfoLevel)

	return &Logger{*log, Fields{}}
}

func (logger *Logger) addMetadata() (log *logrus.Entry) {
	pc, file, line, ok := runtime.Caller(2)

	log = logger.Log.WithFields(logrus.Fields(logger.Fields))

	if ok {
		file = file[strings.LastIndex(file, "/")+1:]

		log = log.WithFields(logrus.Fields{
			"file":   file,
			"line":   line,
			"method": runtime.FuncForPC(pc).Name(),
		})
	}

	return
}

func (logger *Logger) Info(message string) {
	logger.addMetadata().
		Info(message)
}

func (logger *Logger) Error(message string) {
	logger.addMetadata().
		Error(message)
}

func (logger *Logger) Warn(message string) {
	logger.addMetadata().
		Warn(message)
}

func (logger *Logger) Debug(message string) {
	logger.addMetadata().
		Debug(message)
}

func NewContext(ctx context.Context, fields Fields) context.Context {
	return context.WithValue(ctx, loggerKey, fields)
}

func (logger *Logger) WithContext(ctx context.Context) *Logger {
	if ctx == nil {
		return logger
	}

	if fields, ok := ctx.Value(loggerKey).(Fields); ok {
		return &Logger{*log, fields}
	}

	return logger
}
