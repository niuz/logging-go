package logging

import (
	"context"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"strconv"
	"sync"
	"testing"
)

func TestContextLogger_Debug(t *testing.T) {
	SetUp(DebugLevel)
	msg := "TestLogger"
	logger := GetLogger()
	hook := test.NewLocal(&logger.Log)

	logger.Debug(msg)

	assert.Equal(t, 1, len(hook.Entries))
	assert.Equal(t, logrus.DebugLevel, hook.Entries[0].Level)
	assert.Equal(t, msg, hook.Entries[0].Message)
}

func TestLogger_Info(t *testing.T) {
	msg := "TestLogger"
	logger := GetLogger()
	hook := test.NewLocal(&logger.Log)

	logger.Info(msg)

	assert.Equal(t, 1, len(hook.Entries))
	assert.Equal(t, logrus.InfoLevel, hook.Entries[0].Level)
	assert.Equal(t, msg, hook.Entries[0].Message)
}

func TestContextLogger_Warn(t *testing.T) {
	msg := "TestLogger"
	logger := GetLogger()
	hook := test.NewLocal(&logger.Log)

	logger.Warn(msg)

	assert.Equal(t, 1, len(hook.Entries))
	assert.Equal(t, logrus.WarnLevel, hook.Entries[0].Level)
	assert.Equal(t, msg, hook.Entries[0].Message)
}

func TestContextLogger_Error(t *testing.T) {
	msg := "TestLogger"
	logger := GetLogger()
	hook := test.NewLocal(&logger.Log)

	logger.Error(msg)

	assert.Equal(t, 1, len(hook.Entries))
	assert.Equal(t, logrus.ErrorLevel, hook.Entries[0].Level)
	assert.Equal(t, msg, hook.Entries[0].Message)
}

func TestLogger_InfoWithNilContext(t *testing.T) {
	msg := "TestLogger_Info"
	logger := GetLogger().WithContext(nil)
	hook := test.NewLocal(&logger.Log)

	logger.Info(msg)

	assert.Equal(t, 1, len(hook.Entries))
	assert.Equal(t, logrus.InfoLevel, hook.Entries[0].Level)
	assert.Equal(t, msg, hook.Entries[0].Message)
}

func TestLogger_MultiRoutines(t *testing.T) {
	numberOfRoutines := 100
	log := &GetLogger().Log
	hook := test.NewLocal(log)
	var wg sync.WaitGroup
	wg.Add(numberOfRoutines)

	for i := 0; i < numberOfRoutines; i++ {
		s := strconv.Itoa(i)

		go func() {
			defer wg.Done()

			ctx := NewContext(
				context.Background(),
				map[string]interface{}{"correlationId": s})
			var logger = GetLogger().WithContext(ctx)

			logger.Info(s)
		}()
	}

	wg.Wait()

	assert.Equal(t, numberOfRoutines, len(hook.Entries))
	assert.True(t, checkCorrelationIdEqualMsg(hook.Entries), "Seems that logging isn't thread safe")
}

func TestLogger_MultiRoutinesWithInheritLogger(t *testing.T) {
	numberOfRoutines := 100
	log := &GetLogger().Log
	hook := test.NewLocal(log)
	var wg sync.WaitGroup
	wg.Add(numberOfRoutines)

	for i := 0; i < numberOfRoutines; i++ {
		s := strconv.Itoa(i)

		go func() {
			defer wg.Done()

			ctx := NewContext(
				context.Background(),
				map[string]interface{}{"correlationId": s})
			logger := GetLogger()
			contextLogger := logger.WithContext(ctx)

			// Create new routines that use the same logger
			createRoutinesWithLogger(numberOfRoutines, i, logger)

			contextLogger.Info(s)
		}()
	}

	wg.Wait()

	assert.Equal(t, numberOfRoutines+numberOfRoutines*numberOfRoutines, len(hook.Entries))
	assert.True(t, checkCorrelationIdEqualMsg(hook.Entries), "Seems that logging isn't thread safe when logger is inherit")
}

func TestLogger_WithContextWithoutFields(t *testing.T) {
	msg := "TestLogger"
	logger := GetLogger().WithContext(context.Background())
	hook := test.NewLocal(&logger.Log)

	logger.Info(msg)

	assert.Equal(t, 1, len(hook.Entries))
	assert.Equal(t, logrus.InfoLevel, hook.Entries[0].Level)
	assert.Equal(t, msg, hook.Entries[0].Message)
}

func createRoutinesWithLogger(numberOfRoutines int, i int, logger *Logger) {
	var wg sync.WaitGroup
	wg.Add(numberOfRoutines)

	for j := 0; j < numberOfRoutines; j++ {
		t := strconv.Itoa(i) + "- " + strconv.Itoa(j)
		go func() {
			defer wg.Done()

			ctx := NewContext(
				context.Background(),
				map[string]interface{}{"correlationId": t})

			contextLogger := logger.WithContext(ctx)

			contextLogger.Info(t)

		}()
	}

	wg.Wait()
}

func checkCorrelationIdEqualMsg(entries []logrus.Entry) bool {
	for _, entry := range entries {
		if correlationId, ok := entry.Data["correlationId"].(string); ok {
			if correlationId != entry.Message {
				return false
			}
		} else {
			return false
		}

	}

	return true
}
